﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{
    public Vector2 axis;
    public PlayerBehaviour player;

    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        //Debug.Log("x: " + axis.x + " y: " + axis.y);
        if (Input.GetButton("Fire1")){//GetButton, mantienes pulsado y es true
            player.Shoot();//llamamos a la función
        }

        player.SetAxis(axis);
    }
}
