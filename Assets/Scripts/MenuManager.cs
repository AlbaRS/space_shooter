﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){

        SceneManager.LoadScene("Space");
    }

    public void PulsaCreditos(){
        SceneManager.LoadScene("Creditos");
    }
    
    public void PulsaExit(){
        Application.Quit();
    }
}
