﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    /*https://pastebin.com/chlmpsgj*/
    public GameObject canvas;
    private bool isPaused = false;
    
    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape)) {
            ActivatePause();
        } else if (isPaused && Input.GetKeyDown(KeyCode.Escape)){
            pulsaResume();
        }
    }

    public void pulsaResume() {
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void pulsaExit() {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }
    void ActivatePause()
    {
        isPaused = true;

        canvas.SetActive(true);

        Time.timeScale = 0;
    }
}
