﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Vector2 speed = new Vector2 (0, 0);
    public GameObject laserBullet;

    private void Start()
    {
        StartCoroutine (EnemyBehaviour());
    }

    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }

    IEnumerator EnemyBehaviour() {
        while (true) {
            //avanza horizontalmente
            speed.x = -1f;
            speed.y = -1f;

            yield return new WaitForSeconds(1.0f);

            //se para
            speed.x = 0f;
            speed.y = 0f;
            yield return new WaitForSeconds(1.0f);

            speed.x = -1f;
            speed.y = 1f;
            yield return new WaitForSeconds(1.0f);

            speed.x = 0f;
            speed.y = 0f;
            yield return new WaitForSeconds(1.0f);

            //dispara
            Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);

        }
    }
}
