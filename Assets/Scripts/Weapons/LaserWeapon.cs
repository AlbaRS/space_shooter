﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon : Weapon //llamamos al otro script
{
    public GameObject laserBullet;
    public float cadencia;
    public AudioSource laserSound;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);

        laserSound.Play();
    }
}
