﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour//al ser abstract no se puede instanciar, ningún gameobject con este script
{
    public abstract float GetCadencia();

    public abstract void Shoot();
}