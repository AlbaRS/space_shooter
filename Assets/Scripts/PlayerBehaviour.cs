﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public GameObject shipgraphics;

    public ScoreManager scoreManager;

    [SerializeField] Collider2D collider;

    public AudioSource explosionSound;
    public ParticleSystem ps;

    public float shootTime = 0;
    public Weapon weapon;

    public Propeller prop;

    public int lives = 3;

    private bool iamDead = false;

    private void Update()
    {
        if (iamDead) {
            return;
        }

        shootTime+=Time.deltaTime; //se suma tiempo cada frame

        transform.Translate(axis * speed * Time.deltaTime);//movimiento nave time.deltatime -> tiempo que ha pasado entre un frame y otro

        if (transform.position.y > limits.y){
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y){
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }
        if (transform.position.x > limits.x){
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x){
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }
        if (axis.x > 0){
            prop.BlueFire();
        }else if(axis.x < 0){
            prop.RedFire();
        }else{
            prop.Stop();
        }

    }

    public void SetAxis(Vector2 CurrentAxis){
        axis = CurrentAxis;
    }

    public void Shoot(){
        //Debug.Log("Dispara");
        if (shootTime > weapon.GetCadencia()) {
            shootTime = 0f;
            weapon.Shoot();
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Meteor") {
            StartCoroutine(DestroyShip());
        }
    }

    IEnumerator DestroyShip() {
        //bool para verificar que estamos muertos
        iamDead = true;
        //vamos restando vidas
        lives--;
        //desactivar graficos
        shipgraphics.SetActive(false);
        //propgraphics.SetActive(false);

        //desactivamos collider
        collider.enabled = false;

        //Destroy(GetComponent<BoxCollider2D>());
        
        ps.Play();
        explosionSound.Play();

        //desactivar propeller
        prop.gameObject.SetActive(false);


        scoreManager.LoseLife();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Destruir objeto
        //Destroy(this.gameObject);

        if (lives > 0) {
            StartCoroutine(Inmortal());
        }
    }

    IEnumerator Inmortal() {
        //desactivamos muerte
        iamDead = false;
        //activamos gráficos
        shipgraphics.SetActive(true);
        //activamos propellers
        prop.gameObject.SetActive(true);

        for (int i=0; i<15; i++){//bucle de graficos y propellers para que parpadee
            shipgraphics.SetActive(false);
            prop.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            shipgraphics.SetActive(true);
            prop.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        //después de estos segundos, el collider se activará
        collider.enabled = true;
    }


}
