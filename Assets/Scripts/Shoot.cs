﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float speed;
    public Vector2 direction;

    // Update is called once per frame
    void Update(){
        transform.Translate(direction * speed * Time.deltaTime);
    }

    /*void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.tag == "Finish"){
            Destroy(gameObject); //destruye todo el gameobject
            //Destroy(this); //Destruye componente del object
        }
    }*/

    public void OnTriggerEnter2D(Collider2D other)//para detectar la colisión
    {
        if (other.tag == "Finish" ||other.tag == "Meteor")
        {
            Destroy(gameObject);
        }
    }
}
