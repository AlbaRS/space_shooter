﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
    public GameObject meteorPrefab;
    public float timeLauchMeteor;

    //private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(LanzaMeteoritos());
    }

    IEnumerator LanzaMeteoritos()
    {
        while (true)
        {
            Instantiate(meteorPrefab, new Vector3(9, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(timeLauchMeteor);
        }
    }
}
